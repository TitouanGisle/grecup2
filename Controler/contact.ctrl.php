<?php
require_once('../Model/Panier.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/View.class.php');
require_once('../Model/Mail.class.php');

//////////////////////////////////////////////////////////////////////////////
// PARTIE RECUPERATION DES DONNEES
//////////////////////////////////////////////////////////////////////////////
session_start();
$envoi = FALSE;
$traitement = 0; // c'est quoi ça merde ...

// si pas de paramètre on affiche le formulaire, sinon on traite le formulaire
if (!isset($_POST['mail'])) {
  $traitement = 1;
}
else { // message ok
  // on récupère toute les infos du formulaire
  $nom = htmlentities($_POST['nom']);
  $mail = htmlentities($_POST['mail']);
  $objet = htmlentities($_POST['objet']);
  $text = htmlentities($_POST['saisie']);


  //   test d'upload de ficxhier
  $dir = './';
  $fichier  = $dir . basename($_FILES['fichier']['name']);
  $tailleMax = 100000;
  $tailleFichier = filesize($_FILES['fichier']['tmp_name']);
  $extensions = array('.pdf.', '.odt', '.jpg', '.png'); // rajouter d'autres types plus fann_create_standard
  $extension = strchr($_FILES['fichier']['name'], '.');
  //var_dump($fichier);

  $nop = null;
  // verif de securité
  if (!in_array($extension, $extensions)) {
    $nop = '';//veuillez uploader un fichier avec le bon type
  }
  if ($tailleFichier > $tailleMax) {
    $nop = 'c\'est trop gros';
  }
  var_dump($nop);
  if ($nop === null) { // donc pas d'error ;)

    // $fichier = strtr($fichier, é, e); // finir avec plein d'autre carac spéciaux...ç c à a ...
    //$fichier = preg_replace('/([^.a-z0-9]+)/i', '-', $fichier);

    if (move_uploaded_file($_FILES['fichier']['tmp_name'],$fichier))  {// si renvoi TRUE c'est que  c'est bon !!

      echo 'upload OK !';
    }
    else { // donc renvoi FALSE
      var_dump($fichier);
      echo 'echec ...';
    }
  }
  else {
    echo $nop;
  }
  // fin du test d'upload de fichier

  // génération et envoi du mail
  $mail = new Mail($mail, $nom, $text);
  $envoi = $mail->ecrireMailContact($objet);
}

//////////////////////////////////////////////////////////////////////////////
// PARTIE SELECTION DE LA VUE
//////////////////////////////////////////////////////////////////////////////
if ($traitement) {
  $view = new View('../View/contact.view.php');
  $view->show();
}
else {
  $view = new View('../View/confirmation.view.php');
  $view->envoi = $envoi;
  $view->show();
}


 ?>
