<?php
// Controleur formulaire : appelé lors de la validation du panier
// Objectif : pré-remplir un formulaire
require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/View.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');

//////////////////////////////////////////////////////////////////////////////
// PARTIE RECUPERATION DES DONNEES
//////////////////////////////////////////////////////////////////////////////
//au cas où l'on arrive là de manière impromptue: on démarre la session et créé un panier
session_start();
if(!isset($_SESSION['panier'])) {
    $_SESSION['panier'] = new Panier();
}

//mais s'il est vide, on est renvoyé ailleurs
if($_SESSION['panier']->getQuantitePanier() == 0) {
    header('Location: consulterPanier.ctrl.php');
    exit;
}

// toutes les données nécessaires sont dans $_SESSION
$panier = $_SESSION['panier'];

//////////////////////////////////////////////////////////////////////////////
// PARTIE GENERATION DE LA VUE
//////////////////////////////////////////////////////////////////////////////
$view = new View('../View/formulaire.view.php');
$view->transactions = $panier->getTransactions();
$view->show();

?>
