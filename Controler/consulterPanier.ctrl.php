<?php
// Controleur consulterPanier : celui qui est concerné lorsque le visiteur clique sur l'onglet "Panier"
// Objectif : générer la page qui affiche le contenu du panier du client

require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/View.class.php');
session_start();
//////////////////////////////////////////////////////////////////////////////
// PARTIE RECUPERATION DES DONNEES
//////////////////////////////////////////////////////////////////////////////
//récupération des éléments du panier
if (isset($_SESSION['panier'])) {
    $panier = $_SESSION['panier'];
//ou création si on arrive là sans que le panier n'aie été créé, pour une quelconque raison
} else {
    $_SESSION['panier'] = new Panier();
    $panier = $_SESSION['panier'];
}

//////////////////////////////////////////////////////////////////////////////
// PARTIE USAGE DU MODELEa
//////////////////////////////////////////////////////////////////////////////

//cas où l'on arrive sur cette page depuis une suppression
if (isset($_GET['suppression'])) {
    $idASupprimer = htmlentities($_GET['suppression']);

    //on cherche l'id de la transaction à supprimer
    $i = $panier->getIdTransaction($idASupprimer);
    //on enlève la transaction à cette id
    unset($panier->transactions[$i]);
    //puis on 'regroupe' l'array
    $panier->transactions = array_values($panier->transactions);
    //et on met à jour le Panier enregistré
    $_SESSION['panier'] = $panier;
}

//récupération des transactions
$transactions = $panier->getTransactions();

//////////////////////////////////////////////////////////////////////////////
// PARTIE GENERATION DE LA VUE
//////////////Votre demande
////////////////////////////////////////////////////////////////
if ($panier->getQuantitePanier()==0) {
    $view = new View('panierVide.view.php');
    $view->show();

} else {
    $view = new View('panier.view.php');
    $view->transactions = $transactions;
    $view->total = $panier->getTotal();
    $view->show();
}

?>
