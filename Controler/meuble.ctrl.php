<?php
//Génération de l'affichage d'un meuble
//dont l'id est passé via $_GET

require_once('../Model/DAOMeuble.class.php');
require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/View.class.php');
session_start();

//////////////////////////////////////////////////////////////////////////////
// PARTIE RECUPERATION DES DONNEES
//////////////////////////////////////////////////////////////////////////////

// $ini = parse_ini_file('../Config/config.ini');
//gestion des arrivées impromptues
if (!isset($_GET["id"])){
    header('Location: vente.ctrl.php');
    exit;
}

// On récupère l'id du meuble à visionner
$id = htmlentities($_GET["id"]);

/////////////////////////////////////////////////////////////////////////////
// PARTIE USAGE DU MODELE
//////////////////////////////////////////////////////////////////////////////

// On récupère le meuble grâce à la DAO
$DAO = new DAOMeuble();
$meuble = $DAO->getMeuble($id);
$images = $DAO->getImages($id);

//////////////////////////////////////////////////////////////////////////////
// PARTIE GENERATION DE LA VUE
//////////////////////////////////////////////////////////////////////////////

// On crée la vue et on lui passe les attributs du meuble récupéré
  $view = new View('../View/meuble.view.php');

  $view->id = $id;
  $view->intitule = $meuble->intitule;
  $view->categorie = $meuble->categorie;
  $view->vendable = $meuble->vendable;
  $view->louable = $meuble->louable;
  $view->descriptif = $meuble->descriptif;
  $view->prixVente = $meuble->prixVente;
  $view->images = $images;

  $view->show();
?>
