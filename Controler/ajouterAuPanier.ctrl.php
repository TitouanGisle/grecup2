<?php
// Controleur AjouterAuPanier : Il est appele quand on appuie sur le bouton Acheter ou Louer

require_once('../Model/DAOMeuble.class.php');
require_once('../Model/DAOPackage.class.php');
require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/View.class.php');
session_start();

// Recupere les donnees du GET

//si rien dans le get : renvoie vers l'accueil
if(!(isset($_GET["id"]) && isset($_GET["quantite"]))) {
	header('Location: start.ctrl.php');
	exit;
}


// FAIRE DES TESTS D ERREUR
$id = htmlentities($_GET["id"]);
$quantite = (int) htmlentities($_GET["quantite"]);

/////////////////////////////////////////////////////////////////////////////
// PARTIE USAGE DU MODELE
//////////////////////////////////////////////////////////////////////////////

// Recupere le Meuble/le Package avec la DAO
if(isset($_GET["duree"])) {
	$DAO = new DAOPackage();
	$element = $DAO->getPackage($id);
} else {
	$DAO = new DAOMeuble();
	$element = $DAO->getMeuble($id);
}


// Recupere le panier
//$panier = $_SESSION['panier'];
$panier = (isset($_SESSION['panier'])) ? $_SESSION['panier'] : new Panier();
$found = false;

// Cree la transaction
// cas d'un achat :
if (!isset($_GET["dateDebut"])) {
	$transaction = new Transaction($element, $quantite);

} else {
	$dateDebut = new DateTime(htmlentities($_GET["dateDebut"]));
	//cas d'une location
	if (isset($_GET["dateFin"])) {
		$dateFin = new DateTime(htmlentities($_GET["dateFin"]));
	//cas d'un package
	} else {
		$interval = new DateInterval("P".htmlentities($_GET["duree"])."D");
		$dateFin = $dateDebut->add($interval);
	}
	//$dateDebut = date('d/m/Y', strtotime($_GET['dateDebut']));
	//$dateFin = date('d/m/Y', strtotime($_GET['dateFin']));
	$transaction = new Location($element, $quantite, $dateDebut, $dateFin);
}

// si une transaction pour le meme produit existe déjà on rajoute à la quantité
foreach ($panier->transactions as $t) {
	if ($t->memeTransaction($transaction)) {
		$t->quantite+=$transaction->quantite;
		$found = true;
		break;
	}
}

if (!$found) $panier->ajouterTransaction($transaction);

// on enregistre le panier dans les variables de session
$_SESSION['panier'] = $panier;

// Renvoie la page du meuble
if(isset($_GET["duree"])) {
	header('Location: package.ctrl.php?id='.$id);
} else {
	header('Location: meuble.ctrl.php?id='.$id);
}

exit;
?>
