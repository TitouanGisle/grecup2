<?php
require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/View.class.php');
session_start();
$panier = $_SESSION['panier'];

//au cas où l'on arrive là de manière impromptue: on démarre la session et créé un panier
if(!isset($_SESSION['panier'])) {
    $_SESSION['panier'] = new Panier();
}

$panier = $_SESSION['panier'];

//mise à jour des quantité du panier avec les quantités passées en POST
foreach ($panier->transactions as $transaction) {
    $id = $transaction->element->id;
    $transaction->quantite = $_POST[$id];
}

$_SESSION['panier']=$panier;

header('Location: consulterPanier.ctrl.php');
exit;
?>
