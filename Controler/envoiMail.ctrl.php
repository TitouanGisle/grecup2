<?php
// Controleur envoiMail : appelé lors de la validation du formulaire de demande
// Objectif : confirme l'envoi ou non du mail
// s'il a bien été envoyé : vide le panier et
// ajoute les transactions demandées à la base de données

require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/View.class.php');
require_once('../Model/Mail.class.php');
require_once('../Model/DAOTransaction.class.php');

//////////////////////////////////////////////////////////////////////////////
// PARTIE RECUPERATION DES DONNEES
//////////////////////////////////////////////////////////////////////////////
//vérification que l'on n'arrive pas sans le strict minimum
if(!isset($_GET["email"])) {
    header("Location: consulterPanier.ctrl.php");
    exit;
}

// récupération des données du formulaire
$expediteur = htmlentities($_GET["nom"]);
$email = htmlentities($_GET["email"]);
$livraison = htmlentities($_GET["livraison"]);
$adresse = htmlentities($_GET["adresse"]);
$message = htmlentities($_GET["message"]);

// récupération des produits du panier
session_start();
$panier = $_SESSION["panier"];

// génération et envoi du mail
$mail = new Mail($email, $expediteur, $message);
$envoi = $mail->ecrireMailDemande($panier, $livraison, $adresse);

// si le mail a été envoyé
if ($envoi) {
  // ajout des transactions dans la base de données
  $dao = new DAOTransaction();
  $transactions = $panier->getTransactions();
  foreach ($transactions['locations'] as $transaction) {
    $dao->setTransaction($transaction->element->id, $transaction->dateDebut->format('d/m/Y'),
    $transaction->dateFin->format('d/m/Y'), $transaction->quantite, 0);
  }
  foreach ($transactions['ventes'] as $transaction) {
    $dao->setTransaction($transaction->element->id, NULL, NULL, $transaction->quantite, 0);
  }

  // réinitialisation du panier
  $_SESSION['panier'] = new Panier();
}

//////////////////////////////////////////////////////////////////////////////
// PARTIE GENERATION DE LA VUE
//////////////////////////////////////////////////////////////////////////////
// Retour à la page d'accueil
$view = new View('../View/confirmation.view.php');
$view->envoi = $envoi;
$view->show();

?>
