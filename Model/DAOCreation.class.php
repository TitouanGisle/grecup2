<?php

require_once('DAO.class.php');
require_once('Element.class.php');

class DAOCreation extends DAO {

	// Fonction qui retourne la création dont l'id est passé en paramètre
	function getCreation(string $id) {
		try {
			$query = $this->db->query("SELECT * FROM creation WHERE id = '$id'");
			$result = $query->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, "Element");
		}
    catch (PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}

		if (sizeof($result) == 0) {
			die("No value found for '$id'\n");
		}

		return $result[0];
	}

	function getImages(string $id) {
		try {
			$query = $this->db->query("SELECT nomFichierImageCreation FROM imageCreation WHERE idCreation = '$id'");
			$result = $query->fetchAll(PDO::FETCH_COLUMN);
		}
		catch (PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}

		if (sizeof($result) == 0) {
			die("No value found for '$id'\n");
		}

		return $result;
	}

}

?>
