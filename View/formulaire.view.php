r<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/formulaires.css"/>
    <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
    <title>G'recup - Formulaire de demande</title>
  </head>
  <body>
    <?php require_once('header.ctrl.php'); ?>

    <h2>Récapitulatif de votre demande</h2>

    <p>Suite à la validation de ce formulaire, nous vous répondrons par mail dans les
      meilleurs délais afin de convenir d'un accord.</p>
      <div class="container">
      <form id="formulaire" method="get" action="../Controler/envoiMail.ctrl.php">
        <fieldset>
          <legend>Vos coordonnées</legend>
          <table>
            <tr>
              <td><label for="nom">Nom : </label></td>
              <td><input type="text" id="nom" name="nom" placeholder="Votre nom"></td>
            </tr>
            <tr>
              <td><label for="email">Adresse mail de contact : </label></td>
              <td><input type="email" id="mail" name="email" required placeholder="Votre mail"></td>
            </tr>
          </table>
          <hr>
        </fieldset>

        <fieldset>
          <legend>Votre demande</legend>

          <!-- affichage des locations -->
          <?php
            // s'il y a des location on crée un tableau
            if (count($this->transactions['locations']) != 0)  {
          ?>
          <h3>Location</h3>
          <table>
            <tr>
              <th>Intitulé</th>
              <th>Categorie</th>
              <th>Quantité</th>
              <th>Du</th>
              <th>Au</th>
            </tr>
            <?php
              foreach ($this->transactions['locations'] as $transaction) {
                echo "<tr>";
                echo "<td>".$transaction->element->intitule."</td>";
                echo "<td>".$transaction->element->categorie."</td>";
                echo "<td>".$transaction->quantite."</td>";
                echo "<td>".($transaction->dateDebut)->format('d/m/Y')."</td>";
                echo "<td>".($transaction->dateFin)->format('d/m/Y')."</td>";
                echo "</tr>";
              }
             ?>
          </table>
          <?php } // fin du if ?>

          <!-- affichage des ventes -->
          <?php
            // s'il y a des location on crée un tableau
            if (count($this->transactions['ventes']) != 0)  {
          ?>
          <h3>Achat</h3>
          <table>
            <tr>
              <th>Intitulé</th>
              <th>Categorie</th>
              <th>Quantité</th>
              <th>Prix à l'unité</th>
              <th>Prix total</th>
            </tr>
            <?php
              foreach ($this->transactions['ventes'] as $transaction) {
                $quantite = $transaction->quantite;
                $prixU = $transaction->element->prixVente;
                echo "<tr>";
                echo "<td>".$transaction->element->intitule."</td>";
                echo "<td>".$transaction->element->categorie."</td>";
                echo "<td>".$quantite."</td>";
                echo "<td>".$prixU." €</td>";
                echo "<td>".$prixU*$quantite." €</td>";
                echo "</tr>";
              }
             ?>
          </table>
          <?php } // fin du if ?>
          <hr>
        </fieldset>

        <fieldset>
          <legend>Souhaitez-vous être livré ?</legend>
          <div class="livraison">
            <input type="radio" name="livraison" value="oui" id="oui">
            <label for="oui">Oui</label>

            <input type="radio" name="livraison" value="non" checked id="non">
            <label for="non">Non</label>
          </div>

            <label for="adresse">Adresse ou lieu de livraison</label>
            <input type="text" name="adresse" id="adresse" placeholder="" rows="4"></input>
            <br/>
            <hr>
        </fieldset>

        <fieldset>
          <legend>Votre message</legend>
          <textarea id="message" name="message" placeholder="Entrer le message" rows="10"></textarea>
        </fieldset>

        <div class="bouton">
          <input type="submit" value="Valider">
        </div>
      </form>
    </div>
  </body>
</html>
