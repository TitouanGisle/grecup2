<?php
//Génération de l'affichage d'une création
//dont l'id est passée via $_GET

require_once('../Model/DAOCreation.class.php');
require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/View.class.php');
session_start();

//////////////////////////////////////////////////////////////////////////////
// PARTIE RECUPERATION DES DONNEES
//////////////////////////////////////////////////////////////////////////////

// On récupère l'id de la création que l'on veut visionner
if (!isset($_GET["id"])) {
    header('Location: creations.ctrl.php');
    exit;
}
$id = htmlentities($_GET["id"]);

/////////////////////////////////////////////////////////////////////////////
// PARTIE USAGE DU MODELE
//////////////////////////////////////////////////////////////////////////////

// On récupère la création avec la DAO
$crea = new DAOCreation();
$creation = $crea->getCreation($id);
$images = $crea->getImages($id);

//////////////////////////////////////////////////////////////////////////////
// PARTIE GENERATION DE LA VUE
//////////////////////////////////////////////////////////////////////////////

// On crée la vue et on lui passe les paramètres correspondant aux attributs de la création récupérée
  $view = new View('../View/creation.view.php');
  $view->id=$id;
  $view->intitule = $creation->intitule;
  $view->descriptif = $creation->descriptif;
  $view->images = $images;
  $view->show();

?>
