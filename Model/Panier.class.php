<?php
class Panier {
	public $transactions; // Tableau de Transaction

	function __construct() {
		$this->transactions = array();
	}

	function ajouterTransaction(Transaction $transaction) {
		array_push($this->transactions, $transaction);
	}

	function getTransactions()
	// renvoie un tableau contenant un tableau de transactions de type location et un de celles de types vente
	{
		$ventes = array();
		$locations = array();
		$transactions = array();

		foreach ($this->transactions as $transaction) {
			if (isset($transaction->dateDebut)) array_push($locations, $transaction);
			else array_push($ventes, $transaction);
		}

		$transactions['locations'] = $locations;
		$transactions['ventes'] = $ventes;
		return $transactions;
	}

	//renvoie la quantite d'Elements contenus dans le panier
	function getQuantitePanier() {
		$qte = 0;
		foreach($this->transactions as $transaction) {
			$qte+=$transaction->quantite;
		}
		return $qte;
	}

	//renvoie l'index de la transaction comportant un Element dont l'id est entré en paramètre
	function getIdTransaction($id) {
		$i = 0;
		while (isset($this->transactions[$i]) && $this->transactions[$i]->element->id!=$id){
			$i++;
		}
		return $i;
	}

	//renvoie le prix total du panier
	function getTotal() {
		$total = 0;

		foreach($this->transactions as $transaction) {
			if (!$transaction instanceof Location) {
				$total = $total + ($transaction->quantite * $transaction->element->prixVente);
			} else if ($transaction->element instanceof Package) {
				$total = $total + ($transaction->quantite * $transaction->element->prix);
			}
		}
		return $total;
	}
}

?>
