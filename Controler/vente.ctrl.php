<?php
// Controleur vente : celui qui est concerné lorsque le visiteur clique sur l'onglet "Vente"
// Objectif : générer la vue qui affiche tous les meubles disponibles à la vente
require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/DAOVente.class.php');
require_once('../Model/View.class.php');
session_start();

/////////////////////////////////////////////////////////////////////////////
// PARTIE USAGE DU MODELE
//////////////////////////////////////////////////////////////////////////////

$vente = new DAOVente;
/*
$chill = $vente->getVendablesChill();
$bar = $vente->getVendablesBar();
*/

//récupération des meubles vendables
$vendables = $vente->getVendablesByCat();

//////////////////////////////////////////////////////////////////////////////
// PARTIE GENERATION DE LA VUE
//////////////////////////////////////////////////////////////////////////////

// On crée la vue et on lui passe le tableau de meubles vendables récupéré
$view = new View('../View/vente.view.php');
$view->vendables = $vendables;

$view->show();

?>
