<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/confirmation.css"/>
    <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
    <title>G'recup - Demande envoyée</title>
  </head>
  <body>
    <?php require_once('header.ctrl.php'); ?>

    <?php
      // affiche un message de confirmation ou d'erreur
      if ($this->envoi) {
        echo "<p> Votre message a bien été envoyé à G'recup, nous vous répondrons dans les meilleurs délais. </p>";
      }
      else {
        echo "<p> Erreur lors de l'envoi du mail, veuillez réessayer. </p>";
      }
     ?>

  </body>
</html>
