<?php
require_once('Element.class.php');
require_once('DAO.class.php');

class DAOVente extends DAO {
    // Renvoie tous les meubles vendables par catégorie
    function getVendablesByCat() {
        //renvoie un array d'arrays de Meubles
        try {
            //récupération des différentes catégories :
            $categories = ($this->db)->query('SELECT DISTINCT categorie FROM meuble WHERE vendable=1 AND quantite>0')->fetchAll(PDO::FETCH_COLUMN);

            $vendables = array();
            /*chill => banquettesimple, chaiselongue...
            bar => chaiseHaute, minibar...
            enfants => dés en bois, cheval à bascule...*/

            //récupération, pour chaque catégorie, des meubles concernés
            foreach ($categories as $key => $categorie) {
                $sth = ($this->db)->query("SELECT * FROM meuble WHERE categorie='$categorie' AND vendable=1 AND quantite>0");
                $sth->setFetchMode(PDO::FETCH_GROUP|PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Meuble');
                $meubles = $sth->fetchAll();
                $vendables["$categorie"]=$meubles;
            }
            return $vendables;
        }
        catch (PDOException $e) {
            die("Erreur : ".$e->getMessage()."\n");
        }
    }
}

?>
