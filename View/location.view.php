<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../View/CSS/location.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
    <title>G'recup - Location</title>
  </head>
  <body>
    <?php include_once('header.ctrl.php'); ?>

    <!-- haut de page-->
    <div class="header2">

      <div class="imageFond">
        <img id="bandeau" src="../../Images/Tuiles.PNG" alt="Meubles de G'recup sur l'herbe du tram et personnes assises dessus">
      </div>

      <div class="textFond">
        <h1 id="titre">Nos meubles disponibles à la location</h1>
        <p>Ici blabla nos meuble sont fait avec amour blabla lorem ...</p>
      </div>
    </div>
    <!--fin haut de page-->
<div class="transaction">
    <p>Filtrer la recherche</p>
    <form action="../Controler/location.ctrl.php" method="get">
      <div class="conteneur">
        <div class="ligneFiltre">
          <label for="dateDebut">Date de début pour la location : </label>
          <input type="date" name="dateDebut">
        </div>
        <div class="ligneFiltre">
          <label for="dateFin">Date de fin : </label>
          <input type="date" name="dateFin">
        </div>
        <div class="ligneFiltre">
          <label for="categorie">Catégorie : </label>
          <select name="categorie">

            <option value="tous">Tous</option>
    <?php foreach($this->listeCategories as $key => $categorie) {
            echo "<option value=\"$categorie\">$categorie</option>";
    }
    ?>
            </select>
          </div>

        </div>
        <div class="boutonValider">
          <input type="submit" value="Valider">
        </div>
      </form>
    </div>



    <?php
      if (isset($this->erreur)) {
        echo "<p>$this->erreur</p>";
      }
      else if (isset($this->dateDebut) || isset($this->categorie)) {
        echo "<p>Résultats ";
        if (isset($this->dateDebut)) {
          echo "pour la période du $this->dateDebut au $this->dateFin ";
        }
        if (isset($this->categorie)) {
          echo "pour la catégorie $this->categorie";
        }
        echo "</p>";
      }
    ?>

    <section>
      <div class="container">

    <?php
	// Affichage des meubles
    // On boucle sur les meubles louables qui vérifient les conditions entrées
      foreach ($this->meubles as $meuble):
    ?>
    <div class="meuble">
      <div id="photo">
        <a href="meuble.ctrl.php?id=<?=$meuble->id?>">
          <img class="imageMeuble" src="../../Images/<?= $meuble->miniature ?>" alt="<?= $meuble->intitule ?>">
        </a>
      </div>
      <div id="descriptionMeubleGalerie">
          <a href="meuble.ctrl.php?id=<?=$meuble->id?>">
            <h3 align="center" id"titreMeuble"  ><?= $meuble->intitule ?></h3>   <!-- align="center" -->
          </a>
          <p id="description">
            <?=$meuble->descriptif?>
          </p>
          <p class="info" align="right"><a id="plusInfo" href="meuble.ctrl.php?id=<?=$meuble->id?>">Plus d'information</a></p>

        </div>
      </div>
    <?php
      endforeach;
     ?>
    </section>

    <h2 class="titreH2">Packages</h2>

    <section>

      <?php
        // Affichage des packages
          foreach ($this->packages as $package):
        ?>
          <article class="icone">
            <div id="miniature">
              <img src="../../Images/<?= $package->miniature ?>" alt="<?= $package->intitule ?>">
            </div>
            <div class="descriptionPackageIcone">
              <a href="package.ctrl.php?id=<?= $package->id ?>"><?= $package->intitule ?></a>
            </div>
            </article>
        <?php
          endforeach;
         ?>
    </section>

    </div>
  </body>
</html>
