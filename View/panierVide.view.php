<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/panierVide.css"/>
    <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
    <title>G'recup - Panier</title>
  </head>
  <body id="vide">
      <?php require_once('header.ctrl.php') ?>

      <!-- haut de page-->
        <p>Votre panier est pour l'instant vide, mais vous pouvez facilement y remédier !</p>
        <div id="links">
            <p><a href="../Controler/location.ctrl.php">Location</a></p>
            <p><a href="../Controler/vente.ctrl.php">Vente</a></p>
        </div>
    </body>
</html>
