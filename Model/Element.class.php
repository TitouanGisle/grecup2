<?php

class Element {
	public $id;
	public $intitule;
	public $descriptif;
	public $miniature;

	function estDisponible(DateTime $date) {}
}

class Meuble extends Element {
	public $categorie;
	public $louable;
	public $prixVente; // Un meuble est vendable s'il possede un prix de vente different de NULL

	function estVendable() { return $prixVente != NULL; }
}

class Package extends Element {
	public $prixLocation;
	public $dureeLocation; // dateInterval
	public $meubles; // Tableau de Transactions (de type Vente)
}

?>
