<?php
// Controleur vente : celui qui est concerné lorsque le visiteur clique sur l'onglet "Nos créations"
// Objectif : générer la vue qui affiche toutes les créations
require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/DAOCreations.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/View.class.php');
session_start();

/////////////////////////////////////////////////////////////////////////////
// PARTIE USAGE DU MODELE
//////////////////////////////////////////////////////////////////////////////

// On récupère les créations avec la DAO
$crea = new DAOCreations;
$creations = $crea->getCreations();

//////////////////////////////////////////////////////////////////////////////
// PARTIE GENERATION DE LA VUE
//////////////////////////////////////////////////////////////////////////////

// On crée la vue en lui passant le tableau de créations récupéré
$view = new View('../View/creations.view.php');
$view->creations = $creations;
$view->show();

?>
