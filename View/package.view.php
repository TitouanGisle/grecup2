<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
  <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
  <link rel="stylesheet" type="text/css" href="../View/CSS/meuble.css"/>
  <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
  <title>G'recup - <?= $this->intitule ?></title>
</head>
<body id="package">
  <?php require_once('header.ctrl.php') ?>

  <div id="description">
    <div id ="apercu">
      <img src="<?=$this->miniature?>" alt="photo <?= $this->intitule ?>">
    </div>
    <aside>
      <h1><?= $this->intitule ?></h1>
      <p><?= $this->descriptif ?></p>
       <p>Prix : <?= $this->prix ?>€ </p>
    </aside>

    <h2>Ce package contient :</h2>
    <?php var_dump($this->meubles) ?>

    <!-- ajouter la liste des meubles qui composent le packages -->
  </div>
  <div id="transaction">
    <!-- afficher ou non si loubale/achetable -->
    <div id="location">
      <form action="ajouterAuPanier.ctrl.php">
        Du <input type="date" name="dateDebut" > pour <?= $this->duree ?> jours
        <br>
        Quantite : <input type="number" name="quantite" min="1" max="666" value="1">
        <br>
        <input type="submit" value="Ajouter la location au panier">
        <input type="hidden" name="id" value="<?=$this->id?>">
        <input type="hidden" name="duree" value="<?=$this->id?>">
      </form>
    </div>
  </div>
</body>
</html>
