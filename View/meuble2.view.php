<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/meuble.css"/>
    <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
    <title>G'recup - <?= $this->intitule ?></title>
  </head>
  <body id="meuble">
      <?php require_once('header.ctrl.php') ?>
<div class="container">

  <div class="apercu">
        <?php
          foreach ($this->images as $image):
         ?>
        <div class="image">
          <img src="../../Images/<?= $image ?>" alt="<?= $this->intitule ?>">

        </div>
        <?php
          endforeach;
         ?>
    </div>

<div class="description">


  <div id="description">
      <aside>
          <h1><?= $this->intitule ?></h1>
          <p><?= $this->descriptif ?></p>
      </aside>
  </div>

  <div id="transaction">
      <!-- afficher ou non si louable/achetable -->
      <div id="achat">
          <h3>Acheter</h3>
          <p><b>Prix à l'unité : </b><?= $this->prixVente?> €</p>
          <form action="ajouterAuPanier.ctrl.php">
              <input type="hidden" name="id" value="<?=$this->id?>">
              <input type="number" name="quantite" min="1" max="666" value="1">
              <input type="submit" value="Ajouter l'achat au panier">
          </form>
      </div>
      <div id="location">
          <h3>Louer</h3>
          <p>Dès que nous recevrons votre demande, nous vous enverrons un devis.</p>
          <form action="verifDispo.ctrl.php" method="get">
              <label for="dateDebut">Du </label>
              <input type="date" name="dateDebut" required>
              <label for="dateFin"> au </label>
              <input type="date" name="dateFin" required><br/>
              <input type="submit" value="Vérifier les disponibilités">
              <input type="hidden" name="id" value="<?=$this->id?>">
          </form>
      </div>
  </div>
  </div>
</div>
</body>
</html>
