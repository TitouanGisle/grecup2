<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/panier.css"/>
    <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
    <title>G'recup - Panier</title>
  </head>
  <body>
      <?php require_once('header.ctrl.php') ?>

    <!-- haut de page-->
    <h2>Votre panier</h2>
    <form action="actualiserPanier.ctrl.php" method="post">
    <!--Blocs correspondants à des articles du panier-->
<?php
    //si on a des ventes dans le panier:
    if (count($this->transactions['ventes']) > 0) {
        echo "<h4>Ventes : </h4>";
        //on boucle dessus

        foreach ($this->transactions['ventes'] as $vente):
            //on récupère l'élément (meuble ou package)
            $element = $vente->element;
            //et on affiche un encadré avec ses détails
?>
            <article class="item">
                <div id="miniature">
                    <img src="../../Images/<?= $element->miniature ?>" alt="<?= $element->intitule ?>"/>
                </div>
                <div id="descriptionItem">
                    <a href="../Controler/meuble.ctrl.php?id=<?=$element->id?>"><h3><?= $element->intitule ?></h3></a>
                    <div id="prix">
                        <p id="prixU">Prix à l'unité : <?= $element->prixVente ?> €</p>
                        <input type="number" min="1" name="<?=$element->id?>" value="<?= $vente->quantite ?>">
                        <p id="prixT">Sous-total : <?= $element->prixVente * $vente->quantite ?> €</p>
                    </div>
                </div>
                <div id="delete">
                    <a href="consulterPanier.ctrl.php?suppression=<?= $element->id?>">&#10062;</a>
                </div>
            </article>
<?php
endforeach;
}
    //si on a des locations dans le panier:
    if (count($this->transactions['locations']) > 0) {
        echo "<h4>Locations : </h4>";
        //on boucle dessus
        foreach ($this->transactions['locations'] as $location):
            //on récupère l'élément (meuble ou package)
            $element = $location->element;
            //et on affiche un encadré avec ses détails
?>
            <article class="item">
                <div id="miniature">
                    <img src="../../Images/<?= $element->miniature ?>" alt="<?= $element->intitule ?>"/>
                </div>
                <div id="descriptionItem">
                    <a href="../Controler/meuble.ctrl.php?id=<?=$element->id?>"><h3><?= $element->intitule ?></h3></a>
                    <input type="number" min="1" name="<?=$element->id?>" value="<?= $location->quantite ?>">
                    <p id="dates">Du <?= ($location->dateDebut)->format('d/m/Y') ?> au <?= ($location->dateFin)->format('d/m/Y') ?></p>
                </div>
                <div id="delete">
                    <a href="consulterPanier.ctrl.php?suppression=<?= $element->id?>">&#10062;</a>
                </div>
            </article>
<?php
        endforeach;
    }
?>

    <!-- Bas de page -->
            <input type="submit" value="Actualiser mon panier">
        </form>
        <form action="formulaire.ctrl.php" method="post">
            <p id="total">Total : <?=$this->total?> € </p>
            <input type="submit" value="Valider mon panier"/><br/>
        </form>

    </body>
</html>
