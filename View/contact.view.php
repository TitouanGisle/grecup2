<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/formulaires.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/contact.css"/>
    <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
    <title>G'recup - Contact</title>
  </head>
  <body>
    <?php require_once('header.ctrl.php'); ?>
    <header>
      <img id="bandeau" src="../../Images/Chien.jpg" alt="Image d'une chien sur des meubles de G'recup">
    </header>

    <div class="container">

        <h2>Contactez-nous</h2>

      <form id="contact" method="post" action="../Controler/contact.ctrl.php" enctype="multipart/form-data">
        <fieldset>
          <legend>Vos coordonnées</legend>

        </fieldset>
        <table>
          <tr>
            <td><label for="nom">Nom : </label></td>
            <td><input type="text" id="nom" name="nom" placeholder="Votre nom"></td>
          </tr>
          <tr>
            <td><label for="mail">Adresse mail de contact : </label></td>
            <td><input type="email" id="mail" name="mail" required placeholder="Votre mail"></td>
          </tr>
        </table>
        <hr>
        <fieldset>
          <legend>Votre demande</legend>
          <table>
            <tr>
              <td><label for="objet">Objet du message : </label></td>
              <td><input type="text" id="objet" name="objet" placeholder="Entrer l'objet du message"></td>
            </tr>
          </table>

          <label for="saisie">Texte</label><br>
          <textarea id="saisie" name="saisie" placeholder="Entrer le message" style="height:200px"></textarea>

          <div class="upload">
            <label for="uploadFile">Charger une pièce jointe</label>
            <input type="hidden" name="MAX_FILE_SIZE" value="12345">
            <input type="file" name="fichier" value="">
          </div>

        </fieldset>

        <div class="boutons">
            <input type="submit" value="Envoyer mail" >
            <a href="../Controler/start.ctrl.php"><input type="button" name="" value="Annuler"></a>
        </div>
      </form>
    </div>
  </body>
</html>
