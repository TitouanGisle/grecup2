------------------------------------------------
------------------------------------------------
--  INSERT des meubles
------------------------------------------------
------------------------------------------------
INSERT INTO meuble
(id,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("BanqSimp",
	"Chill",
	"Banquette simple",
	1,
	1,
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	90.00,
	8,
	"BanqSimp.PNG");
------------------------------------------------
INSERT INTO meuble
(id,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("BanqAngOuv",
	"Chill",
	"Banquette angle ouvert",
	1,
	1,
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	110.00,
	7,
	"BanqAngOuv.PNG");
------------------------------------------------
INSERT INTO meuble
(id,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("BanqAngFerm",
	"Chill",
	"Banquette angle fermé",
	1,
	1,
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	110.00,
	8,
	"BanqAngFerm.jpg");
------------------------------------------------
INSERT INTO meuble
(id,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("BanqRom",
	"Chill",
	"Banquette romaine",
	1,
	1,
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	335.00,
	2,
	"BanqRom.PNG");
------------------------------------------------
INSERT INTO meuble
(id,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("BancRect",
	"Chill",
	"Banc rectangulaire",
	1,
	1,
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	175.00,
	8,
	"BancRect.PNG");
------------------------------------------------
INSERT INTO meuble
(id,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("BancArrond",
	"Chill",
	"Banc arrondi",
	1,
	1,
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	125.00,
	7,
	"BancArrond.PNG"
);
------------------------------------------------
INSERT INTO meuble
(id,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("ChaiHaut",
	"Bar",
	"Chaise haute",
	1,
	1,
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	80.00,
	19,
	"ChaiHaut.PNG"
);
------------------------------------------------
INSERT INTO meuble
(id,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("TabHaut",
	"Bar",
	"Table haute",
	1,
	1,
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	155.00,
	5,
	"TabHaut.PNG"
);
------------------------------------------------
------------------------------------------------
--  INSERT meubles sur mesure
------------------------------------------------
------------------------------------------------
INSERT INTO meuble
(id,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("Bar",
	"Sur Mesure",
	"Bar transportable – Une Petite Mousse",
	1,
	0,
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	0,
	1,
	"Bar.jpg"
);
------------------------------------------------
INSERT INTO meuble
(id,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("Comptoir",
	"Sur Mesure",
	"Comptoir de vente – Les Jardins de Malissoles",
	1,
	0,
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat 0a pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	0,
	1,
	"Comptoir.jpg"
);
------------------------------------------------
INSERT INTO meuble
(id,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("Debout",
	"Sur Mesure",
	"Mange debout et chaises hautes – La Chouette Company",
	1,
	0,
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat 0a pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	0,
	1,
	"Debout.jpg"
);
------------------------------------------------
INSERT INTO meuble
(id,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("Enseigne",
	"Sur Mesure",
	"Garvure laser enseigne – La Chouette Company",
	1,
	0,
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat 0a pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	0,
	1,
	"Enseigne.jpg"
);
------------------------------------------------
INSERT INTO meuble
(id,categorie,intitule,vendable,louable,descriptif,prixVente,quantite,miniature)
VALUES
("TableReunion",
	"Sur Mesure",
	"Table de réunion – La Chouette Company",
	1,
	0,
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat 0a pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	0,
	1,
	"TableReunion.jpg"
);
------------------------------------------------
------------------------------------------------
--  INSERT images
------------------------------------------------
------------------------------------------------
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("TabHaut","TabHaut.PNG");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("TabHaut","TabHaut1.jpg");
------------------------------------------------
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("ChaiHaut","ChaiHaut.PNG");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("ChaiHaut","ChaiHaut2.jpg");
------------------------------------------------
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BancArrond","BancArrond.PNG");
------------------------------------------------
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BancRect","BancRect.PNG");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BancRect","BancRect1.jpg");
------------------------------------------------
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BanqAngFerm","BanqAngFerm.jpg");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BanqAngFerm","BanqAngFerm1.PNG");
------------------------------------------------
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BanqAngOuv","BanqAngOuv.PNG");
------------------------------------------------
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BanqSimp","BanqSimp.PNG");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BanqSimp","BanqSimp1.jpg");
------------------------------------------------
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BanqRom","BanqRom.PNG");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("BanqRom","BanqRom1.jpg");
------------------------------------------------
------------------------------------------------
--  INSERT creation
------------------------------------------------
------------------------------------------------
INSERT INTO creation
(id,intitule,descriptif)
VALUES
("Banc",
	"Banc et table",
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat 0a pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
);
------------------------------------------------
INSERT INTO creation
(id,intitule,descriptif)
VALUES
("BancBis",
	"Banc",
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat 0a pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
);
------------------------------------------------
INSERT INTO creation
(id,intitule,descriptif)
VALUES
("Cagette",
	"Cagette en palette",
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat 0a pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
);
------------------------------------------------
INSERT INTO creation
(id,intitule,descriptif)
VALUES
("ChaiseHaute",
	"Chaise hautes et mange debout",
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat 0a pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
);
------------------------------------------------
INSERT INTO creation
(id,intitule,descriptif)
VALUES
("ChaisePliante",
	"Chaise pliante rénovée",
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat 0a pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
);
------------------------------------------------
INSERT INTO creation
(id,intitule,descriptif)
VALUES
("Coffre",
	"Coffre « pirate » table de nuit, mini tabouret et plateau de service",
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat 0a pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
);
------------------------------------------------
INSERT INTO creation
(id,intitule,descriptif)
VALUES
("Des",
	"Dés à jouer",
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat 0a pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
);
------------------------------------------------
INSERT INTO creation
(id,intitule,descriptif)
VALUES
("Fauteuil",
	"Fauteuil pneu",
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat 0a pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
);
------------------------------------------------
INSERT INTO creation
(id,intitule,descriptif)
VALUES
("Horloge",
	"Horloge",
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
);
------------------------------------------------
INSERT INTO creation
(id,intitule,descriptif)
VALUES
("Palette",
	"Mini palette",
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
);
------------------------------------------------
INSERT INTO creation
(id,intitule,descriptif)
VALUES
("PlateauDes",
	"Plateau de dés",
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
);
------------------------------------------------
INSERT INTO creation
(id,intitule,descriptif)
VALUES
("Table",
	"Table bistrot pliable",
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
);
------------------------------------------------
------------------------------------------------
--  INSERT image des créations
------------------------------------------------
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Banc","Banc.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Banc","Banc1.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Banc","Banc2.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Banc","Banc3.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("BancBis","BancBis.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("BancBis","BancBis1.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("BancBis","BancBis2.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("BancBis","BancBis3.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("BancBis","BancBis4.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Cagette","Cagette.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Cagette","Cagette1.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Cagette","Cagette2.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("ChaiseHaute","ChaiseHaute.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("ChaiseHaute","ChaiseHaute1.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("ChaiseHaute","ChaiseHaute2.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("ChaisePliante","ChaisePliante.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("ChaisePliante","ChaisePliante1.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("ChaisePliante","ChaisePliante2.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("ChaisePliante","ChaisePliante3.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Coffre","Coffre.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Coffre","Coffre1.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Coffre","Coffre2.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Coffre","Coffre3.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Coffre","Coffre4.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Coffre","Coffre5.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Des","Des.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Des","Des1.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Fauteuil","Fauteuil.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Fauteuil","Fauteuil2.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Fauteuil","Fauteuil3.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Fauteuil","Fauteuil4.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Horloge","Horloge.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Horloge","Horloge1.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Palette","Palette.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Palette","Palette1.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("PlateauDes","PlateauDes.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("PlateauDes","PlateauDes1.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("PlateauDes","PlateauDes2.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("PlateauDes","PlateauDes3.jpg");
------------------------------------------------
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Table","Table.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Table","Table1.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Table","Table2.jpg");
INSERT INTO imageCreation (idCreation, nomFichierImageCreation) VALUES ("Table","Table3.jpg");
------------------------------------------------
------------------------------------------------
--  INSERT image sur mesure
------------------------------------------------
------------------------------------------------
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Bar","Bar.jpg");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Bar","Bar1.jpg");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Bar","Bar2.jpg");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Bar","Bar3.jpg");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Bar","Bar4.jpg");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Bar","Bar5.jpg");
------------------------------------------------
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Comptoir","Comptoir.jpg");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Comptoir","Comptoir1.jpg");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Comptoir","Comptoir2.jpg");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Comptoir","Comptoir3.jpg");
------------------------------------------------
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Debout","Debout.jpg");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Debout","Debout1.jpg");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Debout","Debout2.jpg");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Debout","Debout3.jpg");
------------------------------------------------
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Enseigne","Enseigne.jpg");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Enseigne","Enseigne1.jpg");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Enseigne","Enseigne2.jpg");
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("Enseigne","Enseigne3.jpg");
------------------------------------------------
INSERT INTO image (idMeuble, nomFichierImage) VALUES ("TableReunion","TableReunion.jpg");
------------------------------------------------
------------------------------------------------
--  INSERT packages
------------------------------------------------
------------------------------------------------
INSERT INTO package
(id,intitule,prix,duree,descriptif,miniature)
VALUES
("SmallBar",
	"Small Bar",
	100,
	3,
	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
	"SmallBar.png"
);
INSERT INTO contient
(idPack,idMeuble,quantite)
VALUES
("SmallBar",
	"ChaiHaut",
	2
);
INSERT INTO contient
(idPack,idMeuble,quantite)
VALUES
("SmallBar",
	"TabHaut",
	1
);
