<?php
//Génération de l'affichage d'un package
//dont l'id est passé via $_GET

require_once('../Model/DAOPackage.class.php');
require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/View.class.php');
session_start();

//////////////////////////////////////////////////////////////////////////////
// PARTIE RECUPERATION DES DONNEES
//////////////////////////////////////////////////////////////////////////////

// $ini = parse_ini_file('../Config/config.ini');

//gestion des arrivées impromptues
if (!isset($_GET["id"])){
    header('Location: vente.ctrl.php');
    exit;
}

// On récupère l'id du package à visionner
$id = htmlentities($_GET["id"]);

/////////////////////////////////////////////////////////////////////////////
// PARTIE USAGE DU MODELE
//////////////////////////////////////////////////////////////////////////////

// On récupère le package grâce à la DAO
$DAO = new DAOPackage();
$package = $DAO->getPackage($id);

//////////////////////////////////////////////////////////////////////////////
// PARTIE GENERATION DE LA VUE
//////////////////////////////////////////////////////////////////////////////

// On crée la vue et on lui passe les attributs du package récupéré
  $view = new View('../View/package.view.php');

  $view->id = $id;
  $view->intitule = $package->intitule;
  $view->descriptif = $package->descriptif;
  $view->prix = $package->prix;
  $view->duree = $package->duree;
  $view->miniature = $package->miniature;
  $view->meubles = $package->meubles;

  $view->show();
?>
