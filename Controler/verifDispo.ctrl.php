<?php
//vérification de la disponibilité d'un meuble
//et génération de la page en fonction

require_once('../Model/DAOMeuble.class.php');
require_once('../Model/DAOTransaction.class.php');
require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/Panier.class.php');
require_once('../Model/View.class.php');
session_start();

//////////////////////////////////////////////////////////////////////////////
// PARTIE RECUPERATION DES DONNEES
//////////////////////////////////////////////////////////////////////////////

// $ini = parse_ini_file('../Config/config.ini');
//gestion des arrivées impromptues
if (!isset($_GET["id"])){
    header('Location: vente.ctrl.php');
    exit;
}

// Les dates de début et de fin de la location éventuellement entrées par le visiteur
if (isset($_GET['dateDebut']) && $_GET['dateDebut']!='') {
  $dDebut = date('d/m/Y', strtotime($_GET['dateDebut']));
  if (isset($_GET['dateFin']) && $_GET['dateFin']!='') {
    $dFin = date('d/m/Y', strtotime($_GET['dateFin']));
  }
  else {
    $erreur = 'Veuillez entrer une date de fin.';
  }
}

$id = htmlentities($_GET["id"]);

$DAO = new DAOTransaction();
$qteDispo = $DAO->qteDispo($id, $dDebut, $dFin);
$qteIndispo = $DAO->qteIndispo($id, $dDebut, $dFin);

$DAO = new DAOMeuble();
$meuble = $DAO->getMeuble($id);
$images = $DAO->getImages($id);

if($qteDispo == 0) {
    $view=new View("../View/indispo.view.php");
    $view->id = $id;
    $view->intitule = $meuble->intitule;
    $view->show();
} else {
    $view=new View("../View/dispo.view.php");
    $view->id = $id;
    $view->intitule = $meuble->intitule;
    $view->categorie = $meuble->categorie;
    $view->vendable = $meuble->vendable;
    $view->louable = $meuble->louable;
    $view->descriptif = $meuble->descriptif;
    $view->prixVente = $meuble->prixVente;
    $view->images = $images;
    $view->qteDispo = $qteDispo; $view->qteIndispo = $qteIndispo;
    $view->dDebut = $_GET['dateDebut']; $view->dFin = $_GET['dateFin'];
    $view->show();
}
