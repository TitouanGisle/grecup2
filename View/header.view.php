<header class='header'>
  <a class='img' href="start.ctrl.php">
    <img id="logo" src="../View/Data/logo.jpg" alt="logo grecup">
  </a>

  <div class="vertical">
    <div class="center">

        <h1>
          Un mobilier audacieux - mais confortable - en matériaux de récup' !
        </h1>
      <div class="reseaux">
        <a href="https://www.facebook.com/pages/category/Home-Decor/Grecup-115134398666137/">
            <img class="header" src="../View/Data/logoFacebook.png" alt="logo facebook">
        </a>
        <a href="#">
            <img class="header" src="../View/Data/logoInstagram.png" alt="logo instagram">
        </a>
        <a href="#">
            <img class="header" src="../View/Data/logoPinterest.png" alt="logo instagram">
        </a>
      </div>
    </div>

    <ul>
      <li><a href="../Controler/activites.ctrl.php">Nos activités</a></li>
      <li><a href="../Controler/creations.ctrl.php">Nos créations</a></li>
      <li><a href="../Controler/location.ctrl.php">Location</a></li>
      <li><a href="../Controler/vente.ctrl.php">Vente</a></li>
      <li><a href="../Controler/decouvrir.ctrl.php">À découvrir</a></li>
      <li><a href="../Controler/contact.ctrl.php">Contact</a></li>
      <li><a href="../Controler/consulterPanier.ctrl.php">Panier (<?= $this->qtePanier?>)</a></li>
    </ul>
  </div>
</header>
