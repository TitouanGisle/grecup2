<?php

require_once('DAO.class.php');
require_once('Element.class.php');

class DAOPackage extends DAO {

	// Renvoie le meuble dont l'id est passé en paramètre
	function getPackage(string $id) {
		try {
			$query = $this->db->query("SELECT * FROM package WHERE id = '$id'");
			$package = ($query->fetchAll(PDO::FETCH_CLASS, "Package"))[0];
			$query = $this->db->query("SELECT m.*,c.quantite AS packQuantite FROM meuble m JOIN contenu c ON m.id = c.idMeuble WHERE c.idPack = '$package->id'");
			$lines = $query->fetchAll(PDO::FETCH_ASSOC);
			$transactions = [];
			foreach ($lines as $line) {
				$meuble = new Meuble();
				$meuble->id = $line["id"];
				$meuble->intitule = $line["intitule"];
				$meuble->categorie = $line["categorie"];
				$meuble->vendable = $line["vendable"];
				$meuble->louable = $line["louable"];
				$meuble->descriptif = $line["descriptif"];
				$meuble->prixVente = $line["prixVente"];
				$meuble->quantite = $line["quantite"];
				$meuble->miniature = $line["miniature"];
				$transactions[] = new Transaction($meuble, $line["packQuantite"]);
			}
			$package->meubles = $transactions;
		} catch (PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}

		if (sizeof($package) == 0) {
			die("No value found for '$id'\n");
		}

		return $package;
	}
}

?>
