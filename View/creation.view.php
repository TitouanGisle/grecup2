<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/creation.css"/>
    <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
    <title>G'recup - <?php $this->intitule ?></title>
  </head>
  <body>
      <?php require_once('header.ctrl.php'); ?>

<h2><?= $this->intitule ?></h2>

  <p><?= $this->descriptif ?></p>
  <section class="creation">


  <?php
    foreach ($this->images as $image):
   ?>
    <div class="image">
      <img src="../../Images/<?= $image ?>" alt="<?= $this->intitule ?>">
    </div>


  <?php
    endforeach;
   ?>
   </section>
  </body>
</html>
