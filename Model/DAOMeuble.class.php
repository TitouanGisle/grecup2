<?php

require_once('DAO.class.php');
require_once('Element.class.php');

class DAOMeuble extends DAO {

	// Renvoie le meuble dont l'id est passé en paramètre
	function getMeuble(string $id) {
		try {
			$query = $this->db->query("SELECT * FROM meuble WHERE id = '$id'");
			$result = $query->fetchAll(PDO::FETCH_CLASS, "Meuble");
		} catch (PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}

		if (sizeof($result) == 0) {
			die("No value found for '$id'\n");
		}

		return $result[0];
	}

	function getImages(string $id) {
		try {
			$query = $this->db->query("SELECT nomFichierImage FROM image WHERE idMeuble = '$id'");
			$result = $query->fetchAll(PDO::FETCH_COLUMN);
		}
		catch (PDOException $e) {
			die("PDO Error :".$e->getMessage());
		}

		if (sizeof($result) == 0) {
			die("No value found for '$id'\n");
		}

		return $result;
	}
}

?>
