<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/dispo.css"/>
    <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
    <title>G'recup - <?= $this->intitule ?></title>
  </head>
  <body id="dispo">
      <?php require_once('header.ctrl.php') ?>

      <h2>Ow :( Il semblerait qu'aucun <?= $this->intitule?> ne soit disponible aux dates que vous avez demandées...</h2>
        <p>Préférez-vous <a href="meuble.ctrl.php?id=<?=$this->id?>">saisir de nouvelles dates</a>, <a href="location.ctrl.php">consulter le restes de nos superbes meubles</a>, ou même <a href="contact.ctrl.php"> prendre directement contact avec nous</a> ?</p>
      </form>
</body>
</html>
