<?php
require_once('Element.class.php');
require_once('DAO.class.php');

class DAOCreations extends DAO {

    // Renvoie toutes les créations
    function getCreations() {
      try {
        $sth = ($this->db)->query("SELECT * FROM creation");
        $sth->setFetchMode(PDO::FETCH_GROUP|PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Element');
        $creations = $sth->fetchAll();
        return $creations;
      }
      catch (PDOException $e) {
        die("Erreur : ".$e->getMessage()."\n");
      }
    }
}
?>
