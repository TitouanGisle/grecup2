<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>G'recup</title>
        <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
        <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
        <link rel="stylesheet" type="text/css" href="../View/CSS/homepage.css"/>
        <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
    </head>
    <body>
      <?php require_once('header.ctrl.php'); ?>

      <header>
        <img id="bandeau" src="../../Images/Serre.jpg" alt="Image d'une serre aménagée avec des meubles de G'recup">
      </header>

      <article>
        <h2>Bienvenue !</h2>
        <p>
          Presentation <br>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p>
      </article>

      <article>
        <h2>Le concept</h2>
        <p>
          C'est une rencontre de jeunes gens motivés, qui viennent d’horizons différents et qui apprennent à construire ensembles.
          <br>C'est un volonté commune de transformer et revaloriser certaines matières premières sans valeurs, qui à nos yeux peuvent redevenir du mobiliers utile.
          <br>C'est un partage d’expériences, de savoir faire et de connaissances qui s'harmonisent pour concrétiser un projet ambitieux.
        </p>
      </article>

      <article>
        <h2>Ce que l'on fait</h2>
        <p>
          Nous cherchons à revaloriser toutes sorte de d'objets inutile en l'état mais qui pourrait, sous nos mains habiles, être transformé en plein de choses. Nous travaillons actuellement avec : du bois provenant souvent de vieilles palettes, des pneus usagés, des appareils électroniques hors service... Nous projetons de travailler avec : des copeaux de bois, des bouteilles en verre, des bouchons en lièges... Cette démarche créative fait parfois émerger des idées originales, que nous mettrons en open source pour continuer à cultiver un esprit de partage.
        </p>
      </article>

    </body>
</html>
