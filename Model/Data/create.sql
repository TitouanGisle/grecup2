DROP TABLE IF EXISTS meuble;
DROP TABLE IF EXISTS package;
DROP TABLE IF EXISTS contient;
DROP TABLE IF EXISTS transactions;
DROP TABLE IF EXISTS image;
DROP TABLE IF EXISTS creation;
DROP TABLE IF EXISTS imageCreation;

CREATE TABLE meuble (
	id TEXT PRIMARY KEY,
	intitule TEXT,
	categorie TEXT,
	vendable INTEGER,
	louable INTEGER,
	descriptif TEXT,
	prixVente REAL,
	quantite INTEGER, -- revoir car calc
	miniature TEXT
);

CREATE TABLE creation (
	id TEXT PRIMARY KEY,
	intitule TEXT,
	descriptif TEXT
);

CREATE TABLE imageCreation (
	nomFichierImageCreation TEXT PRIMARY KEY,
	idCreation INTEGER references creation(id)
);

CREATE TABLE image (
	nomFichierImage TEXT PRIMARY KEY,
	idMeuble INTEGER references meuble(id)
);

CREATE TABLE package (
	id TEXT PRIMARY KEY,
	intitule TEXT,
	prix INTEGER,
	duree INTEGER,
	descriptif TEXT,
	miniature TEXT
);

CREATE TABLE contient (
	idPack TEXT references package(id),
	idMeuble TEXT references meuble(id),
	quantite INTEGER
);

CREATE TABLE transactions (
	id INTEGER PRIMARY KEY AUTOINCREMENT,
	idMeuble TEXT references meuble(id),
	dateDebut DATE,
	dateFin DATE,
	quantite INTEGER,
	validee INTEGER
);
