<?php
require_once('Element.class.php');
require_once('DAO.class.php');

class DAOTransaction extends DAO {
	function setTransaction($idMeuble, $dateDebut, $dateFin, $quantite, $validee) {
    	try {
			$query = $this->db->query("SELECT max(id) from transactions");
			$id = $query->fetch(PDO::FETCH_NUM); // renvoie un tableau
			$id = $id[0]+1; // id = maximum + 1
			$requete = "INSERT INTO transactions VALUES ('$id', '$idMeuble', '$dateDebut', '$dateFin', '$quantite', '$validee')";
			$this->db->exec($requete);
		}
		catch (PDOException $e) {
			die("PDO Error : ".$e->getMessage()." sur la requete : ".$requete);
		}
	}

	function qteIndispo($idMeuble, $dateDebut, $dateFin) {
		try {
			//récupération des meubles déjà en transaction aux dates entrées
			$query = $this->db->query("SELECT SUM(quantite) FROM transactions
			WHERE idMeuble='$idMeuble'
			AND ('$dateDebut' < dateFin)  AND  ('$dateFin' > dateDebut)
			AND validee = 1
			GROUP BY '$idMeuble'");

			$qteIndispo = $query->fetch(PDO::FETCH_NUM);

			return $qteIndispo[0];
		}
		catch (PDOException $e) {
			die("PDO Error : ".$e->getMessage());
		}
	}

	function qteDispo($idMeuble, $dateDebut, $dateFin) {
		try {
			//récupération des meubles déjà en transactions aux dates entrées
			$qteIndispo = $this->qteIndispo($idMeuble, $dateDebut, $dateFin);

			//récupération de la quantité de meuble totale
			$query2 = $this->db->query("SELECT quantite FROM meuble WHERE id = '$idMeuble'");
			$qteTotale = $query2->fetch(PDO::FETCH_NUM);

			//retour de la différence
			return $qteTotale[0]-$qteIndispo;
		}
		catch (PDOException $e) {
			die("PDO Error : ".$e->getMessage());
		}
	}
}

?>
