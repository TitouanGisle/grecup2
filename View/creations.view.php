<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../View/CSS/creations1.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
    <title>G'recup - Nos créations</title>
  </head>
  <body>
      <?php require_once('header.ctrl.php') ?>

    <!-- haut de page-->
    <div class="header2">

      <div class="imageFond">
        <img id="bandeau" src="../../Images/Impression.jpg" alt="Meubles de G'recup sur les rails de tram avec un style impressionniste">
      </div>

      <div class="textFond">
        <h1 id="titre">Nos créations</h1>
        <p>Ici blabla nos meuble sont fait avec amour blabla lorem ...</p>
      </div>
    </div>

    <p>Si vous êtes intéressé.e par une de nos créations, n'hésitez pas à nous formuler une demande via notre <a href="../Controler/contact.ctrl.php">formulaire de contact</a>.</p>
    <div class="container">

    <?php
        // Boucle sur chaque création
        foreach ($this->creations as $creation):
    ?>
      <div class="creation">

        <div id="photo">
        <a href="creation.ctrl.php?id=<?=$creation->id?>">
          <img class="imageMeuble" src="../../Images/<?=$creation->id?>.jpg" alt="<?=$creation->intitule?>">
        </a>
        </div>

        <div id="descriptionMeubleGalerie">
          <a href="creation.ctrl.php?id=<?=$creation->id?>">
            <h3 align="center" id"titreMeuble"><?=$creation->intitule?></h3>
          </a>
          <p id="description">
            <?=$creation->descriptif?>
          </p>
        </div>

      </div>

    <?php
    endforeach;
    ?>
  </div>
  </body>
</html>
