<?php

require_once('../Model/Panier.class.php');
require_once('../Model/Element.class.php');
require_once('../Model/Transaction.class.php');
require_once('../Model/View.class.php');

// On récupère la quantité d'objets dans le panier
$qtePanier=$_SESSION['panier']->getQuantitePanier();

// On génère la vue du header en lui passant la quantité
$view = new View('header.view.php');
$view->qtePanier = $qtePanier;
$view->show();
?>
