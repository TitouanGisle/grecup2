<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
    <link rel="stylesheet" type="text/css" href="../View/CSS/dispo.css"/>
    <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
    <title>G'recup<?= $this->intitule ?></title>
  </head>
  <body id="dispo">
      <?php require_once('header.ctrl.php') ?>

      <p><?=$this->qteDispo?> <?=$this->intitule?> sont disponibles entre le <?=$this->dDebut?> et le <?=$this->dFin?></p>
      <p>Indispos: <?=$this->qteIndispo?></p>
      <form action="ajouterAuPanier.ctrl.php" method="get">
          <label for="quantite">Combien en voulez-vous ?</label>
          <input type="number" name="quantite" value="1" min="1" max="<?= $this->qteDispo?>">
          <input type="hidden" name="id" value="<?=$this->id?>">
          <input type="hidden" name="dateDebut" value="<?=$this->dDebut?>">
          <input type="hidden" name="dateFin" value="<?=$this->dFin?>">
          <input type="submit" value="Ajouter au panier">
      </form>
</body>
</html>
