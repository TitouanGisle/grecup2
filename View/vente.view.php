<!DOCTYPE html>
<html lang="fr" dir="ltr">
<head>
  <meta charset="utf-8">
  <link rel="stylesheet" type="text/css" href="../View/CSS/vente.css"/>
  <link rel="stylesheet" type="text/css" href="../View/CSS/body.css"/>
  <link rel="stylesheet" type="text/css" href="../View/CSS/header.css"/>
  <link href="https://fonts.googleapis.com/css?family=Cabin" rel="stylesheet">
  <title>G'recup - Vente</title>
</head>
<body>
  <?php require_once('header.ctrl.php'); ?>

  <!-- haut de page-->
  <div class="header2">

    <div class="imageFond">
      <img id="bandeau" src="../../Images/Serre2.jpg" alt="Image d'une serre aménagée avec des meubles de G'recup">
    </div>

    <div class="textFond">
      <h1 id="titre">Nos meubles disponibles à l'achat</h1>
      <p>Ici blabla nos meuble sont fait avec amour blabla lorem ...</p>
    </div>
  </div>
<!--fin haut de page-->
  <div class="container">


  <?php
  // Boucle sur chaque catégorie
  foreach ($this->vendables as $categorie=>$meubles):
    ?>


    <div class="container2">
      <div class="titreH2">
         <h2> <?=$categorie?> </h2> <!--style="margin: 10px 20px 10px 30px;" -->
      </div>
    <div class="container3">
    <?php
    //boucle sur chaque meuble de cette catégorie
    foreach ($meubles as $meuble):
      ?>

      <div class="meuble">
          <div id="photo">
          <a href="meuble.ctrl.php?id=<?=$meuble->id?>">
            <img class="imageMeuble" src="../../Images/<?=$meuble->miniature?>" alt="<?=$meuble->intitule?>">
          </a>
          </div>
          <div id="descriptionMeubleGalerie">
            <a href="meuble.ctrl.php?id=<?=$meuble->id?>">
             <h3 align="center" id"titreMeuble"  ><?=$meuble->intitule?></h3>   <!-- align="center" -->
            </a>
            <p id="description">
              <?=$meuble->descriptif?>
            </p>
            <p class="info" align="right"><a id="plusInfo" href="meuble.ctrl.php?id=<?=$meuble->id?>">Plus d'information</a></p>
            <p id="prix" align="right" ><?=$meuble->prixVente?> €</p>
          </div>
      </div>
      <?php
    endforeach;
      ?>
      </div>
    </div>
    <?php
  endforeach;
  ?>
</div>
 </body>
</html>
