<?php
require_once('Element.class.php');
require_once('DAO.class.php');

class DAOLocation extends DAO {

	// Renvoie tous les meubles louables pour les critères éventuellement entrés par le visiteur qui sont passés sous la forme d'un tableau $parametres
	function getLouables($parametres) {
		try {
			if (array_key_exists("dateDebut", $parametres)) $dateDebut = $parametres["dateDebut"];
			if (array_key_exists("dateFin", $parametres)) $dateFin = $parametres["dateFin"];
			if (array_key_exists("categorie", $parametres)) $categorie = $parametres["categorie"];

			if (isset($dateDebut) && isset($dateFin) && isset($categorie)) {
				$sth = ($this->db)->query("SELECT m.id,m.intitule,m.categorie,m.louable,m.descriptif,COALESCE(m.quantite-sum(t.quantite),m.quantite) FROM meuble m
					LEFT JOIN transactions t ON m.id = t.id AND t.dateDebut > '$dateDebut' AND t.dateFin < '$dateFin'
					WHERE m.louable=1 AND m.categorie='$categorie' AND t.validee=1 GROUP BY m.id");
				$sth->setFetchMode(PDO::FETCH_GROUP|PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Meuble');
				$louables = $sth->fetchAll();
			} else if (isset($dateDebut) && isset($dateFin)) {
				$sth = ($this->db)->query("SELECT m.id,m.intitule,m.categorie,m.louable,m.descriptif,COALESCE(m.quantite-sum(t.quantite),m.quantite) FROM meuble m
					LEFT JOIN transactions t ON m.id = t.id AND t.dateDebut > '$dateDebut' AND t.dateFin < '$dateFin'
					WHERE m.louable=1 AND t.validee=1 GROUP BY m.id");
				$sth->setFetchMode(PDO::FETCH_GROUP|PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Meuble');
				$louables = $sth->fetchAll();
			} else if (isset($categorie)) {
				$sth = ($this->db)->query("SELECT * FROM meuble WHERE louable=1 AND categorie='$categorie' AND quantite>0");
				$sth->setFetchMode(PDO::FETCH_GROUP|PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Meuble');
				$louables = $sth->fetchAll();
			} else {
				$sth = ($this->db)->query("SELECT * FROM meuble WHERE louable=1 AND quantite>0");
				$sth->setFetchMode(PDO::FETCH_GROUP|PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Meuble');
				$louables = $sth->fetchAll();
			}

			return $louables;
		} catch (PDOException $e) {
			die("Erreur : ".$e->getMessage()."\n");
		}
	}

	function getPackages($parametres) {
		try {
			if (array_key_exists("dateDebut", $parametres)) $dateDebut = $parametres["dateDebut"];
			if (array_key_exists("dateFin", $parametres)) $dateFin = $parametres["dateFin"];
			if (array_key_exists("categorie", $parametres)) $categorie = $parametres["categorie"];

			$sth = $this->db->query("SELECT p.* FROM package p JOIN contient c ON
				p.id = c.idPack JOIN meuble m ON m.id = c.idMeuble
				WHERE m.quantite > 0  AND m.louable = 1 GROUP BY p.id");
			$sth->setFetchMode(PDO::FETCH_GROUP|PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Package');
			$packages = $sth->fetchAll();

			return $packages;
		} catch (PDOException $e) {
			die("Erreur : ".$e->getMessage()."\n");
		}
	}

	//renvoie un array de string des catégories
	function getCategories() {
		$categories = ($this->db)->query('SELECT DISTINCT categorie FROM meuble WHERE louable=1 AND quantite>0')->fetchAll(PDO::FETCH_COLUMN);
		return $categories;
	}
}

?>
