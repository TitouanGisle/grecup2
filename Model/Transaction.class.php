<?php

class Transaction {
	// Une transaction de base est une vente

	public $element;
	public $quantite;
	public $validee; // vrai si la transaction a ete validee par l'equipe, faux si non ou si elle n'a pas encore été traitée

	function __construct(Element $element, int $quantite) {
		$this->element = $element;
		$this->quantite = $quantite;
	}

	function memeTransaction($other) {
		if (isset($other->dateDebut)) {
			return false;
		} else {
			return $other->element == $this->element;
		}
	}
}

class Location extends Transaction {
	// La date fin est fixe si l'element est un package

	public $dateDebut; // DateTime
	public $dateFin; // DateTime

	function __construct(Element $element, int $quantite, $dateDebut, $dateFin) {
		$this->element = $element;
		$this->quantite = $quantite;
		$this->dateDebut = $dateDebut;
		$this->dateFin = $dateFin;
	}

	function memeTransaction($other) {
		if (!isset($other->dateDebut)) {
			return false;
		} else {
			return $other->element == $this->element
				&& $other->dateDebut == $this->dateDebut
				&& $other->dateFin == $this->dateFin;
		}
	}
}

?>
